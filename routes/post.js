const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const Post = mongoose.model("Post")

//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWY5ZWVjYzE3ZDgwNDU4YTBkMGY4OWUiLCJpYXQiOjE1OTM0NDAxMTR9.YZATch3QFz7IbBUl0OjIz3Uf5sa6yVzOvFW5We7fWTg
router.post('/createpost',requireLogin,(req,res)=>{
    const {title,body} = req.body
    
    if(!title || !body ) {
        return res.status(422).json({error:"Please add All the fields"})
    }
     
    
    req.user.password = undefined
    const post = new Post({
        title, //if title:title we can write it like title
        body,
        postedBy:req.user
    })
    post.save().then(result=>{
        res.json({post:result , user:req.user})
    })
    .catch(err=>{
        console.log(err)
    })
})



router.get('/mypost',requireLogin,(req,res)=>{
    Post.find({postedBy:req.user._id})
    .populate("PostedBy","_id name")
    .then(mypost=>{
        res.json({mypost})
    })
    .catch(err=>{
        console.log(err)
    })
})


router.put('/like',requireLogin,(req,res)=>{
    Post.findByIdAndUpdate(req.body.postId,{
        $push:{likes:req.user._id}
    },{
        new:true
    }).exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }
        else{
            res.json(result)
        }
    })
})


router.put('/unlike',requireLogin,(req,res)=>{
    Post.findByIdAndUpdate(req.body.postId,{
        $pull:{likes:req.user._id}
    },{
        new:true
    }).exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }
        else{
            res.json(result)
        }
    })
})



router.delete('/deletePost/:postId',requireLogin,(req,res)=>{
    Post.findOne({_id:req.params.postId})
    .populate("postedBy","_id")
    .exec((err,post)=>{
        if(err || !post){
            return res.status(422).json({error:err})
        }
        if(post.postedBy._id.toString() === req.user._id.toString()){
            post.remove()
            .then(result=>{
                res.json({result})
            }).catch(err=>{
                console.log(err)
            })
        }
    })
})


module.exports = router 